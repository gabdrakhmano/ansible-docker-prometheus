Задания со * (звездочкой) не обязательны к выполнению (и соответственно не были выполнены :D).

Prometheus:
1. Запустить Prometheus.
1. Настроить мониторинг для инстанса, на котором запущен Prometheus.
1. *Настроить Alertmanager на отправку оповещений в Telegram.
1. *Настроить basic auth для Alertmanager и Prometheus.


Grafana:
1. Запустить Grafana.
1. Добавить дашборд node-exporter и источник данных из первого задания.
1. *Сделать channel для алертинга в телеграм или почту и настроить алерт на потребление RAM выше 85%


Использование:
1. Поправить inventory/hosts.yml (добавить хосты)
1. Проверить соединение - запустить ping_test.sh
1. Запустить установку - запустить run_playbook.sh
1. Проверить результат:
* http://server:9090 - Prometheus UI
* http://server:9093 - Alertmanager UI (м.б. не нужен если есть Grafana)
* http://server:3000 - Grafana UI
